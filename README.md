# Testing Framework for E2E and API Testing

This repository contains a testing framework for end-to-end (E2E) testing and API testing using Rest Assured. It integrates Allure reporting to generate detailed test reports with screenshots for failed tests.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
    - [Running Tests](#running-tests)
    - [Generating Allure Reports](#generating-allure-reports)
    - [Viewing Allure Reports](#viewing-allure-reports)

## Prerequisites

- Java 11 or higher
- Maven 3.6 or higher

## Dependencies

Dependencies are managed through `pom.xml`. Key dependencies include:

- Playwright for browser automation
- JUnit5 for test management
- Allure for reporting
- RestAssured for API testing
- Checkstyle for code quality

## Installation

1. **Clone the repository**:

    ```sh
    git clone https://code.europa.eu/simpl/simpl-open/testing/simpl-e2e-testing
    cd simpl-e2e-testing
    ```

2. **Install dependencies**:

    ```sh
    mvn clean install
    ```

## Usage

### Running Tests

1. **Run all tests**:

    ```sh
    mvn clean test
    ```

2. **Run a specific test**:

    ```sh
    mvn -Dtest=ApplicationInfoTest#registrationForDataSpace test
    ```

### Generating Allure Reports

1. **Install Instructions**

For details see reference [here](https://allurereport.org/docs/install/) MacOS / Windows / Linux.

2. **Generate Allure report**:

    ```sh
        allure generate --clean
        allure open
    ```

3. **Run allure server on MacOS**:

    ```bash
        allure serve -h localhost
    ```

### Viewing Allure Reports

1. **Serve Allure report**:

    ```sh
    mvn allure:serve
    ```

package com.tsystems.pages;

import com.microsoft.playwright.Page;
import com.common.Messages;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HomePage extends BasePage {

    private String expectedSdCreationTitleText = "SD-Creation Wizard";

    private String sdCreationTitleField = "//*[@class=\"headline\"]";
    private String selfDescriptionTextField = "//h2";
    private String getStartedButton = "//*[@class=\"button\"]";
    private String imprintLinkSelector = "//a[normalize-space()='Imprint']";
    private String dataProtectionLinkSelector = "//a[contains(@href, 'privacy-policy')]";

    private String in4StepsTextField = "//*[@class=\"heroTitle\"]";
    private String selectGaiaXObjectTextField = "//p[normalize-space()='Select Gaia-X Object']";
    private String aTailoredInputFormIsGeneratedTextField = "//p[normalize-space()='A Tailored Input Form is Generated']";
    private String enterDetailsForYourInstanceTextField = "//p[normalize-space()='Enter Details for Your Instance']";
    private String exportSelfDescriptionTextField = "//p[normalize-space()='Export Self-Description']";

    private String selectGaiaXObjectImage = "//img[@src='../../assets/images/select.svg']";
    private String aTailoredInputFormIsGeneratedImage = "//img[@src='../../assets/images/form.svg']";
    private String enterDetailsForYourInstanceImage = "//img[@src='../../assets/images/generate.svg']";
    private String exportSelfDescriptionImage = "//img[@src='../../assets/images/export.svg']";


    public HomePage(Page page) {
        super(page);
    }

    public void navigate(String user, String password) {
        page.navigate("https://" + user + ":" + password + "@" + homepage_url);
    }

    public void getPageElements() {
        page.waitForSelector(sdCreationTitleField);
        page.waitForSelector(selfDescriptionTextField);
        page.waitForSelector(getStartedButton);
        page.waitForSelector(imprintLinkSelector);
        page.waitForSelector(dataProtectionLinkSelector);
        page.waitForSelector(in4StepsTextField);
        page.waitForSelector(selectGaiaXObjectTextField);
        page.waitForSelector(aTailoredInputFormIsGeneratedTextField);
        page.waitForSelector(enterDetailsForYourInstanceTextField);
        page.waitForSelector(exportSelfDescriptionTextField);
        page.waitForSelector(selectGaiaXObjectImage);
        page.waitForSelector(aTailoredInputFormIsGeneratedImage);
        page.waitForSelector(enterDetailsForYourInstanceImage);
        page.waitForSelector(exportSelfDescriptionImage);
    }

    public String getSdCreationTitleText() {
        return page.waitForSelector(sdCreationTitleField).innerText();
    }

    public void checkElementsOnHomePage() {
        getPageElements();
        assertEquals(expectedSdCreationTitleText, getSdCreationTitleText(), Messages.getLoginAttemptSuccessfullMessage());
    }
    public void clickGetStartedButton() {
        page.waitForSelector(getStartedButton).click();
    }

    public void clickImprintLink() { page.waitForSelector(imprintLinkSelector).click(); }

    public void clickDataProtectionLink() {
        page.waitForSelector(dataProtectionLinkSelector).click();
    }
}
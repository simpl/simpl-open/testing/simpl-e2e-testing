package com.tsystems.pages;

import com.microsoft.playwright.Page;
import com.common.Messages;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ImprintPage extends BasePage {

    private String expectedImprintPageTitleText = "Contact data of the provider in accordance with section § 5 of the German Telemedia Act (Telemediengesetz/TMG):";
    private String imprintPageTitleField = "//strong[contains(text(),'Contact data of the provider in accordance with se')]";
    private String cookieBoxAcceptAllButton = "(//a[@role='button'][normalize-space()='Accept all'])[1]";


    public ImprintPage(Page page) {
        super(page);
    }

    public void navigate() {
        page.navigate(imprint_url);
    }

    public void getPageElements() {
        page.waitForSelector(imprintPageTitleField);
    }

    public String getImprintPageTitleText() {
        return page.waitForSelector(imprintPageTitleField).innerText();
    }

    public void checkElementsOnImprintPage() {
        getPageElements();
        assertEquals(expectedImprintPageTitleText, getImprintPageTitleText(), Messages.getLoginAttemptSuccessfullMessage());
    }

    public void clickCookieBoxAcceptAllButton() {
        page.waitForSelector(cookieBoxAcceptAllButton);
        page.click(cookieBoxAcceptAllButton); }
}
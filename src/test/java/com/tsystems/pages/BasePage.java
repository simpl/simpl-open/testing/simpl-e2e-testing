package com.tsystems.pages;

import com.common.ConfigLoader;
import com.microsoft.playwright.Page;

public class BasePage {
    protected Page page;
    protected String homepage_url;
    protected String imprint_url;

    public BasePage(Page page) {
        this.page = page;
        this.homepage_url = ConfigLoader.getHomePageUrl();
        this.imprint_url = ConfigLoader.getImprintUrl();
    }
}

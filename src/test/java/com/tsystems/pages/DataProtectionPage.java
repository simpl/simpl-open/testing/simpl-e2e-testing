package com.tsystems.pages;

import com.microsoft.playwright.Page;
import com.common.Messages;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataProtectionPage extends BasePage {

    private String expectedDataProtectionPageTitleText = "The protection of your data at eco – Association of the Internet Industry";
    private String dataProtectionPageTitleField = "//strong[contains(text(),'The protection of your data at eco – Association o')]";


    public DataProtectionPage(Page page) {
        super(page);
    }

    public void getPageElements() {
        page.waitForSelector(dataProtectionPageTitleField);
    }

    public String getDataProtectionTitleText() {
        return page.waitForSelector(dataProtectionPageTitleField).innerText();
    }

    public void checkElementsOnImprintPage() {
        getPageElements();
        assertEquals(expectedDataProtectionPageTitleText, getDataProtectionTitleText(), Messages.getLoginAttemptSuccessfullMessage());
    }
}
package com.tsystems.tests;

import com.common.tests.BaseTest;
import com.common.extensions.AllurePlaywrightExtension;
import com.tsystems.pages.HomePage;
import com.tsystems.pages.ImprintPage;
import io.qameta.allure.*;
import io.qameta.allure.junit5.AllureJunit5;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith({AllureJunit5.class, AllurePlaywrightExtension.class})
public class HomePageTest extends BaseTest {

   private HomePage homePage;
   private ImprintPage imprintPage;
   private String wizardUser = getEnvVariable("USER_WIZARD", "default-username");
   private String wizardPassword = getEnvVariable("PWD_WIZARD", "default-password");


   @BeforeEach
   public void setUpEach() {
      homePage = new HomePage(page);
      imprintPage = new ImprintPage(page);
   }

   @Test
   @Story("SIMPL-1127 Test automation of home page in SD Creation Wizard")
   @DisplayName("SD-Creation Wizard Home Page Test")
   @Description("Application login verification and checking elements on HomePage")
   @Severity(SeverityLevel.CRITICAL)
   public void loggingInAndCheckElementsOnHomePage() {
      homePage.navigate(wizardUser,wizardPassword);
      homePage.checkElementsOnHomePage();
      imprintPage.navigate();
      imprintPage.clickCookieBoxAcceptAllButton();
      imprintPage.checkElementsOnImprintPage();
   }
}
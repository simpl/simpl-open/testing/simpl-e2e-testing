package com.aruba.pages;

import com.common.ConfigLoader;
import com.microsoft.playwright.Page;

public class BasePage {
    protected Page page;
    protected String baseUrl;

    public BasePage(Page page) {
        this.page = page;
        this.baseUrl = ConfigLoader.getBaseUrl();
    }
}

package com.aruba.pages;

import com.microsoft.playwright.Page;

public class ApplicationRequestPage extends BasePage {

    private String emailAddressField = "//input[@type='email']";
    private String organizationNameField = "//input[@formcontrolname='organization']";
    private String participantTypeSelector = ".mat-mdc-select-placeholder";
    private String createCredentialsButton = ".mdc-button__label";

    public ApplicationRequestPage(Page page) {
        super(page);
    }

    public void navigate() {
        page.navigate(baseUrl + "application/request");
    }

    public void getPageElements() {
        page.waitForSelector(emailAddressField);
        page.waitForSelector(organizationNameField);
        page.waitForSelector(participantTypeSelector);
    }

    public void fillEmailAddress(String emailAddressData) {
       page.fill(emailAddressField, emailAddressData);
    }

    public void fillOrganization(String organizationData) {
        page.fill(organizationNameField, organizationData);
    }
    public void selectParticipantType(String participantTypeValue) {
        page.click(participantTypeSelector);
        page.click("//span[normalize-space()='" + participantTypeValue + "']");
    }

    public void clickCreateCredentialsButton() {
        page.waitForSelector(createCredentialsButton).click();
    }

}

package com.aruba.pages;

import com.microsoft.playwright.Page;

public class ApplicationInfoPage extends BasePage {

    private String registerForThisDataSpaceButton = ".mdc-button__label";

    public ApplicationInfoPage(Page page) {
        super(page);
    }

    public void navigate() {
        page.navigate(baseUrl + "/application/info");
    }

    public void setRegisterForThisDataSpaceButton() {
        page.waitForSelector(registerForThisDataSpaceButton).click();
    }
}



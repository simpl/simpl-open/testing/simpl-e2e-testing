package com.aruba.pages;

import com.microsoft.playwright.Page;

public class ApplicationRequestSuccessPage extends BasePage {

    private String successInfoText = "//h6";

    public ApplicationRequestSuccessPage(Page page) {
        super(page);
    }

    public String getSuccessInfoText() {
        return page.waitForSelector(successInfoText).innerText();
    }

}

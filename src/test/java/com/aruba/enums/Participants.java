package com.aruba.enums;

public enum Participants {
    CONSUMER("Consumer"),
    APPLICATION_PROVIDER("Application Provider"),
    DATA_PROVIDER("Data Provider"),
    INFRASTRUCTURE_PROVIDER("Infrastructure Provider");

    private final String value;

    Participants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

package com.aruba.tests;

import com.common.Messages;
import com.common.extensions.AllurePlaywrightExtension;
import com.aruba.pages.ApplicationInfoPage;
import com.aruba.pages.ApplicationRequestPage;
import com.aruba.pages.ApplicationRequestSuccessPage;
import com.aruba.enums.Participants;
import com.common.tests.BaseTest;
import com.settings.RandomDataGenerator;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit5.AllureJunit5;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AllureJunit5.class, AllurePlaywrightExtension.class})
public class ApplicationInfoTest extends BaseTest {

   private ApplicationInfoPage applicationInfoPage;
   private ApplicationRequestPage applicationRequestPage;
   private ApplicationRequestSuccessPage applicationRequestSuccessPage;

   @BeforeEach
   public void setUpEach() {
      applicationInfoPage = new ApplicationInfoPage(page);
      applicationRequestPage = new ApplicationRequestPage(page);
      applicationRequestSuccessPage = new ApplicationRequestSuccessPage(page);
   }

   @Test
   @DisplayName("Registration for DataSpace")
   @Description("Verify registration of organization")
   @Severity(SeverityLevel.CRITICAL)
   public void registrationForDataSpace() {
      String randomEmail = RandomDataGenerator.getRandomEmail();
      String randomOrgName = RandomDataGenerator.getRandomString(10);

      applicationInfoPage.navigate();
      applicationInfoPage.setRegisterForThisDataSpaceButton();
      applicationRequestPage.getPageElements();
      applicationRequestPage.fillEmailAddress(randomEmail);
      applicationRequestPage.fillOrganization(randomOrgName);
      applicationRequestPage.selectParticipantType(Participants.APPLICATION_PROVIDER.getValue());
      applicationRequestPage.clickCreateCredentialsButton();

      assertEquals(Messages.getAccountCreatedSuccessfullyMessage(), applicationRequestSuccessPage.getSuccessInfoText());
   }

   @Test
   @DisplayName("Registration for DataSpace Twice with Same Data")
   @Description("Verify registration of organization twice with same data")
   @Severity(SeverityLevel.CRITICAL)
   public void registrationForDataSpaceTwiceWithSameData() {
      String randomEmail = RandomDataGenerator.getRandomEmail();
      String randomOrgName = RandomDataGenerator.getRandomString(10);

      // First registration attempt
      applicationInfoPage.navigate();
      applicationInfoPage.setRegisterForThisDataSpaceButton();
      applicationRequestPage.getPageElements();
      applicationRequestPage.fillEmailAddress(randomEmail);
      applicationRequestPage.fillOrganization(randomOrgName);
      applicationRequestPage.selectParticipantType(Participants.APPLICATION_PROVIDER.getValue());
      applicationRequestPage.clickCreateCredentialsButton();
      assertEquals(Messages.getAccountCreatedSuccessfullyMessage(), applicationRequestSuccessPage.getSuccessInfoText());

      // Second registration attempt with the same data
      applicationInfoPage.navigate();
      applicationInfoPage.setRegisterForThisDataSpaceButton();
      applicationRequestPage.getPageElements();
      applicationRequestPage.fillEmailAddress(randomEmail);
      applicationRequestPage.fillOrganization(randomOrgName);
      applicationRequestPage.selectParticipantType(Participants.APPLICATION_PROVIDER.getValue());
      applicationRequestPage.clickCreateCredentialsButton();
      assertEquals(Messages.getAccountCreatedSuccessfullyMessage(), applicationRequestSuccessPage.getSuccessInfoText());
   }

   @Test
   public void registrationTestApi() {
      String randomEmail = RandomDataGenerator.getRandomEmail();
      String randomOrgName = RandomDataGenerator.getRandomString(10);
      String requestBody = "{\"email\":\""+randomEmail+"\",\"organization\":\""+randomOrgName+"\"," +
              "\"participantType\":\"DATA_PROVIDER\"}";
      Response postResponse = given()
              .contentType("application/json")
              .body(requestBody)
              .when()
              .post("/public/user-api/onboardingRequest")
              .then()
              .statusCode(201)
              .body("firstName", equalTo("frodo"))
              .body("lastName", equalTo("baggins"))
              .body("email", equalTo(randomEmail))
              .body("id", matchesPattern("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"))
              .extract().response();

      // Extract the ID or any other relevant information from the POST response
      String userId = postResponse.jsonPath().getString("id");
      System.out.println(userId);
   }
}

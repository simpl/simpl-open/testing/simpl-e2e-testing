package com.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

public class Messages {

    private static final Properties properties = new Properties();

    static {
        // Determine language code from Locale, default to English if not found
        String languageCode = Locale.getDefault().getLanguage();
        String fileName = "success_messages_en.properties"; // Default to English

        if (languageCode.equals("de")) {
            fileName = "success_messages_de.properties"; // German
        } else if (languageCode.equals("fr")) {
            fileName = "success_messages_fr.properties"; // French
        }

        try (InputStream inputStream = Messages.class.getResourceAsStream("/" + fileName)) {
            if (inputStream == null) {
                throw new IOException("Property file '" + fileName + "' not found in the classpath");
            }
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            // Handle loading failure as appropriate
        }
    }

    public static String getAccountCreatedSuccessfullyMessage() {
        return properties.getProperty("ACCOUNT_CREATED_SUCCESSFULLY");
    }

    public static String getLoginAttemptSuccessfullMessage() {
        return properties.getProperty("LOGIN_ATTEMPT_COMPLETED_SUCCESSFULLY");
    }
}
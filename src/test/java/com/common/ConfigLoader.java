package com.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {

    private static Properties properties = new Properties();

    public static void load(String env) {
        String propertiesFile = "config/" + env + ".properties";
        try (InputStream input = ConfigLoader.class.getClassLoader().getResourceAsStream(propertiesFile)) {
            if (input == null) {
                throw new IllegalArgumentException("Could not find properties file: " + propertiesFile);
            }
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Failed to load properties file: " + propertiesFile);
        }
    }

    public static String getBaseUrl() {
        return properties.getProperty("base.url");
    }

    public static String getBaseApiUrl() {
        return properties.getProperty("base.url.api");
    }

    public static String getHomePageUrl() {
        return properties.getProperty("homepage.url");
    }

    public static String getImprintUrl() {
        return properties.getProperty("imprint.url");
    }
}
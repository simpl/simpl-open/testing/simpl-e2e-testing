package com.common.tests;

import com.common.ConfigLoader;
import com.microsoft.playwright.*;
import io.github.cdimascio.dotenv.Dotenv;
import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.util.HashMap;

public class BaseTest {
    protected static Playwright playwright;
    protected static Browser browser;
    protected static BrowserContext context;
    protected static Page page;
    protected static Dotenv dotenv;

    static {
        try {
            dotenv = Dotenv.configure().directory(".").load(); // Ensure the directory is set correctly
        } catch (Exception e) {
            System.err.println("Warning: Could not load .env file. Falling back to system environment variables.");
            dotenv = Dotenv.configure().ignoreIfMalformed().ignoreIfMissing().load();
        }
    }

    protected static String getEnvVariable(String key, String defaultValue) {
        String value = System.getenv(key); // Check if the variable is set in the environment
        if (value == null) {
            value = dotenv.get(key); // Fallback to .env file
        }
        return value != null ? value : defaultValue; // Fallback to default value if not set
    }

    @BeforeAll
    public static void setUp() {
        // Load the configuration for the specified environment
        String env = System.getProperty("env", "dev");  // Default to 'dev' if not specified
        ConfigLoader.load(env);
        // REST ASSURED SET-UP
        RestAssured.baseURI = ConfigLoader.getBaseApiUrl();
        playwright = Playwright.create();
        boolean isHeadless = Boolean.parseBoolean(System.getProperty("headless", "false"));
        BrowserType.LaunchOptions options = new BrowserType.LaunchOptions()
                .setHeadless(isHeadless)
                .setEnv(new HashMap<String, String>() {{
                    put("MOON_URL", "http://moon.aerokube.local");  // Update this with your Moon URL
                }});

        browser = playwright.chromium().launch(options);
        context = browser.newContext();
        page = context.newPage();
    }

    @AfterAll
    public static void tearDown() {
        page.close();
        context.close();
        browser.close();
        playwright.close();
    }

    public Page getPage() {
        return page;
    }
}

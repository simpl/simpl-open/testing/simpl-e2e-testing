package com.common.extensions;

import com.settings.PlaywrightFactory;
import com.microsoft.playwright.Page;
import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AllurePlaywrightExtension implements BeforeEachCallback, AfterEachCallback, AfterTestExecutionCallback {

    private static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(AllurePlaywrightExtension.class);
    private static final String PAGE_KEY = "PAGE";

    @Override
    public void beforeEach(ExtensionContext context) {
        Page page = PlaywrightFactory.createPage();
        context.getStore(NAMESPACE).put(PAGE_KEY, page);
    }

    @Override
    public void afterEach(ExtensionContext context) {
        Page page = context.getStore(NAMESPACE).get(PAGE_KEY, Page.class);
        if (page != null) {
            page.close();
        }
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        boolean testFailed = context.getExecutionException().isPresent();
        if (testFailed) {
            Page page = context.getStore(NAMESPACE).get(PAGE_KEY, Page.class);
            if (page != null) {
                takeScreenshot(context.getDisplayName(), page);
            }
        }
    }

    @Attachment(value = "{testName} - screenshot", type = "image/png")
    public byte[] takeScreenshot(String testName, Page page) {
        try {
            Path screenshotPath = Paths.get("target", "screenshots", testName + ".png");
            Files.createDirectories(screenshotPath.getParent());
            page.screenshot(new Page.ScreenshotOptions().setPath(screenshotPath).setFullPage(true));
            return Files.readAllBytes(screenshotPath);
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }
}

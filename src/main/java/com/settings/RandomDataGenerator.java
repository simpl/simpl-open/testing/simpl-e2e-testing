package com.settings;

import java.util.Random;

public class RandomDataGenerator {

    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyz";
    private static final String[] DOMAINS = {
            "example.com", "test.com", "mail.com", "random.org", "domain.net"
    };

    private static final Random RANDOM = new Random();

    public static String getRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(CHARACTERS.charAt(RANDOM.nextInt(CHARACTERS.length())));
        }
        return sb.toString();
    }

    public static String getRandomEmail() {
        String localPart = getRandomString(8);
        String domain = DOMAINS[RANDOM.nextInt(DOMAINS.length)];
        return localPart + "@" + domain;
    }


}
